## GFFToJSON - A template based solution to parsing GTF/GFF files.

### Overview
GFFToJSON is a solution to the frustrating task of parsing the many variations found within GTF or GFF files from various versions or sources. This program can convert any GTF or GFF file into a standard JSON format. Instead of modifying the program to handle variations in GFF format, a JSON template is used to describe the layout of the GFF file, which metadata to include, and how to parse them.

This program is meant as an intermediary step for analyses using GFF files, especially those using GFFs from multiple sources or spanning large time periods. Once in JSON format the GFF data can be directly imported as a data structure using the standard library in most computer languages. This simplifies all downstream programs, as they only need to handle a consistent data structure. Furthermore, this actually reduces the size of the file needed to store the annotation data.

Finally while not required for parsing three other programs are offered to help with standardizing GFF input files.
These are:
1. add_missing_annotation_lines.py — This program will add gene and optionally transcript annotation lines to a GTF/GFF file with only exon annotations.
2. fix_exon_numbering.py — This program will add extra data in the metadata section of the GTF/GFF file in case the exon ordering is not given or mislabeled.
3. rename_NCBI_chromosomes.py — Substitutes the standard names for chromosomes for NCBI chromosome accessions.

##### If you use this program in a research paper, please cite:

Weirick, T., John, D., and Uchida, S.: Resolving the problem of multiple accessions of the same transcript deposited across various public databases. Brief Bioinform. [Accepted]. 

### Basic Usage
Using executing the programs using --help will list all options for the programs in GFFtoJSON
> python gfftojson.py --schema  <schema.json> --gtf <organism.gtf>

Note: Schemas for a number of popular databases are available in the schemas folder.

###### Example:
Running the example below will create a file JSON file named “Homo_sapiens.GRCh38.79.gtf.h250.gtf.json”
> python gfftojson.py                                       \
>     --schema schemas/ENSEMBL_v75plus_schema.json          \
>     --gtf test_files/Homo_sapiens.GRCh38.79.gtf.h250.gtf

### Creating a Schema Template

A number of popular annotation sources are included in the folder schemas. However, if you wish to modify or create your own, below is an explanation on how templates are formatted.

Schema templates have two main components, “data_relations” and the “data_schema”. The data_relations section defines the relationships between the various attribute types and the data_schema describes how to obtain the data for the objects described in data_relations.

Relationships are defined in the data_relations section as:
If you see X then add it to the data structure level Y with the found using value Z.

> “X”: [“Y”, “Z”,  []]

Finally data located in the list at the end used recursively as explained in the previous sentence.

> “X”: [“Y”, “Z”,  [“X1”: [“Y1”, “Z1”,  []]]]

data_schema object should use one of the keys defined in data_relations. The values contained must be named with a key and can be another data_schema object data_relations, an integer, or a regular expression.  data_schema objects will be treated recursively, integers (starting from 0) will use the position in a list of values split by tabs, and a regex will take return the text matching the regex.

###### For example from the following line:
> 1    havana    gene    11869    14409    .    +    .    gene_id "ENSG00000223972"; gene_biotype "transcribed_unprocessed_pseudogene";

###### With the schema:
> "gene": {
>     "gene_id \"(.*?)\";": {
>         "gene_biotype": "gene_biotype \"(.*?)\";",
>         "chromosome": 0,
>         "source": 1,
>         …

###### Will extract:

> "gene": {
>     " ENSG00000223972": {
>         "gene_biotype": "transcribed_unprocessed_pseudogene",
>         "chromosome": “1”,
>         "source": “havana”,
>         …


### Support Programs.

##### add_missing_annotation_lines.py

> python add_missing_annotation_lines.py  --file example.gff --add_transcript [True|False]  (prints to standard output)

Note: If --add_transcript is true this will add genes and transcripts, if false will add only genes.

Example:

>python src/add_missing_annotation_lines.py \
>--file test_files/NONCODEv4_human_lncRNA.gtf.h250.gtf > outfile.gtf

##### fix_exon_numbering.py

> python fix_exon_numbering.py --file example.gff (prints to standard output)

Example:

> python fix_exon_numbering.py \
--file test_files/NONCODEv4_human_lncRNA.gtf.h250.gtf > outfile.gtf


##### rename_NCBI_chromosomes.py

> python rename_NCBI_chromosomes.py [some_ncbi_gff_file.gff] (prints to standard output)


Example:

> python rename_NCBI_chromosomes.py some_ncbi_gff_file.gff > outfile.gff



