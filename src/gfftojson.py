"""
This program can convert any GTF or GFF file into a JSON format. Parsing is determined by a JSON template passed with 
the GFF file.

If you use this program in a research paper, please cite:  
"""
from __future__ import print_function
import argparse
import re
import json


def parselinedatatojson(data_struct_template, line):

    """

    :param data_struct_template:
    :param line:
    :return:
    """

    out_data_struct = {}

    for data_struct_key, data_struct_values in data_struct_template.items():

        assert (type(data_struct_values) == str or type(data_struct_values) == dict or
                type(data_struct_values) == int or type(data_struct_values) == unicode), type(data_struct_values)

        assert data_struct_key not in out_data_struct

        if type(data_struct_values) == int:
            out_data_struct[data_struct_key] = line.split("\t")[data_struct_values]
        elif type(data_struct_values) == str or type(data_struct_values) == unicode:

            try:
                out_data_struct[data_struct_key] = re.findall(data_struct_values, line)[0]
            except IndexError: 
                out_data_struct[data_struct_key] = ""
                print("WARNING: regex not found: ", data_struct_values, line)

        elif type(data_struct_values) == dict:
            out_data_struct[data_struct_key] = {}

    return out_data_struct


def getdatastructlevel(data_struct_template, data_relations, data_struct, data_type, line):
    """
    Getting passed a list of length three or empty. If not empty elements 1 and 2 will be strings and the third
    elements a list with a format similar in the same format as previously listed.
    for each key value
    :param data_struct:
    :param data_relations:
    :param line:
    :return:
    """
    data_type = data_relations[0]
    regex_match = []
    regex_choices = data_relations[1].split("||")
    choice_cnt = 0
    while regex_match == []:
        #print(regex_choices[choice_cnt])
        regex_match = re.findall(regex_choices[choice_cnt], line)
        #regex_choices[choice_cnt]
        #print(regex_match)
        choice_cnt += 1

    regex_match = regex_match[0]
    if not data_relations[-1]:

        if regex_match not in data_struct[data_type]:
            value_data = parselinedatatojson(data_struct_template[data_type].values()[0], line)
            data_struct[data_type].update({regex_match: value_data})

    else:

        if regex_match in data_struct[data_type]:
            sub_data_struct = data_struct[data_type][regex_match]
            sub_data_struct_template = data_struct_template[data_type].values()[0]
            sub_data_relations = data_relations[2]
            getdatastructlevel(sub_data_struct_template, sub_data_relations, sub_data_struct, data_type, line)


parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('--gtf',      dest='gtf',      required=True,  help="A GTF or GFF file.")
parser.add_argument('--schema',   dest='schema',   required=True,  help="A valid JSON template file explaining how the GFF file should be parsed.")
parser.add_argument('--out_name', dest='out_name', required=False, default=False,
                       help='The name of the output json file. Default is the input file name with .json appended.')

args = parser.parse_args()

json_schema = open(args.schema)
json_obj = json.load(json_schema)
gtf_file = open(args.gtf)

data_struct_template = json_obj["data_schema"]
data_relations = json_obj["data_relations"]

data_struct = {}
for key in data_struct_template.keys():
    data_struct.update({key: {}})

for line in gtf_file:
    if line[0] != "#":

        csv = line.split("\t")
        data_type = csv[2]
        if data_type in data_relations:
            tmp_data_relations = data_relations[data_type]
            #try:
            getdatastructlevel(data_struct_template, tmp_data_relations, data_struct, data_type, line)
            #except IndexError:
            #    print(line)
                
# Output file to working directory or out_name path.

if args.out_name:
    out_file_name = args.out_name
else:
    out_file_name = args.gtf.split("/")[-1] + ".json"
 
f_obj = open( out_file_name, 'w')
f_obj.write(json.dumps(data_struct, sort_keys=True, indent=4, separators=(',', ': ')))
f_obj.close()

