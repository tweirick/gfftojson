from __future__ import print_function
import argparse
import json
import re
import os


def reverse_readline(filename, buf_size=8192):
    """a generator that returns the lines of a file in reverse order"""
    #http://stackoverflow.com/questions/2301789/read-a-file-in-reverse-order-using-python

    with open(filename) as fh:
        segment = None
        offset = 0
        fh.seek(0, os.SEEK_END)
        total_size = remaining_size = fh.tell()
        while remaining_size > 0:
            offset = min(total_size, offset + buf_size)
            fh.seek(-offset, os.SEEK_END)
            buffer = fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # the first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # if the previous chunk starts right from the beginning of line
                # do not concact the segment to the last line of new chunk
                # instead, yield the segment first
                if buffer[-1] is not '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if len(lines[index]):
                    yield lines[index]
        yield segment


parser = argparse.ArgumentParser()
parser.add_argument('--file', dest='file', required=True, help="")
parser.add_argument('--add_transcript', dest='add_transcript', help="")
args = parser.parse_args()

#resource_json = json.loads(open(args.resource_file).read())

file_name = args.file
add_transcript = args.add_transcript

new_feature = "gene"
feature_regex = 'gene_id "(.*?)";'
sub_feature_list = []
transcript_feature = "transcript"
transcript_regex = 'transcript_id "(.*?)";'
previous_feature_id = None
transcript_previous_feature_id = None
attribute_delimiter = " ;"
#tac file
out_list = []
bound_list = []
transcript_bound_list = []

for line in reverse_readline(file_name):

    spln = line.split("\t")
    feature_id = re.findall(feature_regex, line)[0]

    if add_transcript:
        transcript_feature_id = re.findall(transcript_regex, line)[0]

        if transcript_previous_feature_id is not None and transcript_feature_id != transcript_previous_feature_id:
            transcript_bound_list = sorted(transcript_bound_list)

            out_list.append(
                "\t".join(
                    (
                        chromosome, source, 'transcript',
                        str(transcript_bound_list[0]),
                        str(transcript_bound_list[-1]),
                        previous_frame,
                        previous_strand,
                        '.',
                        'gene_id "'+previous_feature_id+'"; '+'transcript_id "'+transcript_previous_feature_id+'";'
                    )
                )
            )

            #out_list.append(
            #    "\t".join((chromosome, source, 'transcript',
            #               str(transcript_bound_list[0]), str(transcript_bound_list[-1]),
            #               transcript_previous_feature_id, transcript_feature_id)))
            transcript_bound_list = []

    if previous_feature_id is not None and feature_id != previous_feature_id:
        bound_list = sorted(bound_list)
        #out_list.append(feature_id)
        out_list.append("\t".join((chromosome, source, 'gene',
            str(bound_list[0]),
            str(bound_list[-1]),
            previous_frame, previous_strand, '.',
            'gene_id "'+previous_feature_id+'";'))
        )
        bound_list = []

    chromosome = spln[0]
    source = spln[1]
    start = spln[3]
    stop = spln[4]
    bound_list.append(int(start))
    bound_list.append(int(stop))
    previous_feature_id = feature_id
    previous_frame = spln[5]
    previous_strand = spln[6]


    if add_transcript:
        transcript_chromosome = spln[0]
        transcript_source = spln[1]
        transcript_start = spln[3]
        transcript_stop = spln[4]
        transcript_bound_list.append(int(start))
        transcript_bound_list.append(int(stop))
        transcript_previous_feature_id = transcript_feature_id

    out_list.append(line.strip())


if add_transcript:
    transcript_bound_list = sorted(transcript_bound_list)
    out_list.append("\t".join((chromosome, source, 'transcript', str(transcript_bound_list[0]),
        str(transcript_bound_list[-1]), previous_frame, previous_strand, '.',
        'gene_id "'+previous_feature_id+'"; '+'transcript_id "'+transcript_previous_feature_id+'";')))
    transcript_bound_list = []


bound_list = sorted(bound_list)


out_list.append(
    "\t".join((chromosome, source, 'gene', 
    str(bound_list[0]),
    str(bound_list[-1]), 
    previous_frame, 
    previous_strand, '.',
    'gene_id "'+previous_feature_id+'";'))
)

print("\n".join(reversed(out_list)))

