from __future__ import print_function
import sys


irregular_chr = {
    "NC_000023": "X",
    "NC_000024": "Y",
    "NC_012920": "M"
}

omit_feature_set = set([
'CDS',
'region',
'long_terminal_repeat',
'match',
'cDNA_match',
'D_loop']
)

rename_feature_dict = {
    'primary_transcript': 'transcript',
    'ncRNA': 'transcript',
    'mRNA': 'transcript',
    'rRNA': 'transcript',
    'tRNA': 'transcript',
    'V_gene_segment': 'transcript',
    'D_gene_segment': 'transcript',
    'C_gene_segment': 'transcript',
    'J_gene_segment': 'transcript'
}

print_set = set(( 'gene', 'transcript', 'exon' ))


for line in open(sys.argv[1]):
    if line[0] != "#":
        spln = line.split("\t")

        chromo_ac = spln[0].split(".")[0]

        if chromo_ac in irregular_chr:
            chromosome = irregular_chr[chromo_ac]
        elif "NC_0" in chromo_ac:
            chromosome = str(int(chromo_ac.strip("NC_0")))
        else:
            chromosome = False
        
        if chromosome:

            if spln[2] in rename_feature_dict:
                spln[2] = rename_feature_dict[spln[2]]

            if spln[2] in print_set:
                chromosome+"\t"+"\t".join(spln[1:])
                print(chromosome+"\t"+"\t".join(spln[1:]).strip("\n"))



