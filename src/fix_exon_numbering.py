import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--file', dest='file', required=True, help="")
args = parser.parse_args()

for line in open(args.file):

    spln = line.strip().split("\t")

    if not line[0] == "#":

        if spln[2] == "transcript" or spln[2] == "gene":
           exon_cnt = 1
           print(line.strip())
        elif spln[2] == "exon":
           print(line.strip()+' exon_number "'+str(exon_cnt)+'";')
           exon_cnt += 1
